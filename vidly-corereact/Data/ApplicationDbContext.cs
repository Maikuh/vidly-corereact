﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Proxies;
using vidly_corereact.Models;

namespace vidly_corereact.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer("Server=.\\SQLEXPRESS;Database=vidly_corereact;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<vidly_corereact.Models.Genre> Genre { get; set; }

        public DbSet<vidly_corereact.Models.Customer> Customer { get; set; }

        public DbSet<vidly_corereact.Models.Movie> Movie { get; set; }

        public DbSet<vidly_corereact.Models.Rental> Rental { get; set; }
    }
}
