﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vidly_corereact.Data;

namespace vidly_corereact.DTOs
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public string Token { get; set; }

        public UserDTO(ApplicationUser user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Username = user.UserName;
        }

        public UserDTO(ApplicationUser user, string token)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Username = user.UserName;
            Token = token;
        }
    }
}
