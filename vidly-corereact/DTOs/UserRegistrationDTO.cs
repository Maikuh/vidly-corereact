﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vidly_corereact.Data;

namespace vidly_corereact.DTOs
{
    public class UserRegistrationDTO
    {
        public ApplicationUser User { get; set; }
        public string Password { get; set; }
    }
}
