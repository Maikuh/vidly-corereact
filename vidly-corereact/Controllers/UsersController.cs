﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using vidly_corereact.Data;
using vidly_corereact.DTOs;
using vidly_corereact.Services;

namespace vidly_corereact.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private readonly ApplicationDbContext _context;

        public UsersController(IUserService userService, ApplicationDbContext context)
        {
            _userService = userService;
            _context = context;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] LoginDTO loginDto)
        {
            var user = await _userService.Authenticate(loginDto);

            if (user == null)
                return BadRequest(new { message = "Usuario o contraseña incorrecta" });

            return Ok(user);
        }

        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] UserRegistrationDTO dto)
        {
            var result = await _userService.Create(dto);

            if (result is UserDTO)
                return Ok(result);
            else
                return BadRequest(new { errors = result });
        }

        [HttpPut("{id}"), Authorize("Admin")]
        public async Task<ActionResult> Update([FromBody] UserRegistrationDTO dto)
        {
            var user = await _userService.Update(dto);

            if (user is string)
                return BadRequest(new { message = user });
            else
                return Ok(user);
        }

    }
}