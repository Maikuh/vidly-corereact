﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using vidly_corereact.Data;
using vidly_corereact.DTOs;

namespace vidly_corereact.Services
{
    public interface IUserService
    {
        Task<UserDTO> Authenticate(LoginDTO loginDto);
        Task<dynamic> Create(UserRegistrationDTO dto);
        Task<dynamic> Update(UserRegistrationDTO dto);
    }

    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AppSettings _appSettings;

        public UserService(UserManager<ApplicationUser> userManager, IOptions<AppSettings> appSettings)
        {
            _userManager = userManager;
            _appSettings = appSettings.Value;
        }

        public async Task<UserDTO> Authenticate(LoginDTO loginDto)
        {
            var user = await _userManager.FindByNameAsync(loginDto.Username);

            // Check if user exists or if passwords don't match
            if (user == null || !await _userManager.CheckPasswordAsync(user, loginDto.Password))
                return null;

            // Configure token generation. Can be changed.
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JwtSecretOrKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id)
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            UserDTO userDto = new UserDTO(user, tokenHandler.WriteToken(token));
            userDto.Roles = await _userManager.GetRolesAsync(user);

            return userDto;
        }

        public async Task<dynamic> Create(UserRegistrationDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Password))
                return "Password is required";

            if (await _userManager.FindByNameAsync(dto.User.UserName) != null)
                return "The user \'" + dto.User.UserName + "\' already exists";
            else if (await _userManager.FindByEmailAsync(dto.User.Email) != null)
                return "The email address is already being used";

            var result = await _userManager.CreateAsync(dto.User, dto.Password);

            if (result.Succeeded)
                return new UserDTO(dto.User);

            return result.Errors.Select(x => x.Description).ToList();
        }

        public async Task<dynamic> Update(UserRegistrationDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Password))
                return "Password is required";

            if (await _userManager.FindByNameAsync(dto.User.UserName) != null)
                return "The user \'" + dto.User.UserName + "\' already exists";
            else if (await _userManager.FindByEmailAsync(dto.User.Email) != null)
                return "The email address is already being used";

            var user = await _userManager.FindByIdAsync(dto.User.Id);
            user.FirstName = dto.User.FirstName;
            user.LastName = dto.User.LastName;
            user.UserName = dto.User.UserName;
            user.Email = dto.User.Email;
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, dto.Password); ;

            var result = await _userManager.UpdateAsync(dto.User);

            if (result.Succeeded)
                return new UserDTO(user);

            return null;
        }
    }
}
