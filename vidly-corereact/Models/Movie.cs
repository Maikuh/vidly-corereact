﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace vidly_corereact.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required, MinLength(5), MaxLength(255)]
        public string Title { get; set; }

        [Required, Range(0, 255)]
        public int NumberInStock { get; set; }

        [Required, Range(0, 255)]
        public double DailyRentalRate { get; set; }

        [Required]
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
    }
}
