﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace vidly_corereact.Models
{
    public class Rental
    {
        public int Id { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        [Required]
        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        [Required]
        public DateTime DateOut { get; set; } = DateTime.Now;

        public DateTime DateReturned { get; set; }

        [Range(0, 255)]
        public double RentalFee { get; set; }
    }
}
