﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace vidly_corereact.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required, MinLength(5), MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public string Phone { get; set; }
        public bool isGold { get; set; }
    }
}
